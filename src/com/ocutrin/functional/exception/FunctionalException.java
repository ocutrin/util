package com.ocutrin.functional.exception;

/**
 * @author ocutrin
 *
 */
public class FunctionalException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 5879851382620079275L;

    /**
     * 
     */
    public FunctionalException() {
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public FunctionalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
	super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * @param message
     * @param cause
     */
    public FunctionalException(String message, Throwable cause) {
	super(message, cause);
    }

    /**
     * @param message
     */
    public FunctionalException(String message) {
	super(message);
    }

    /**
     * @param cause
     */
    public FunctionalException(Throwable cause) {
	super(cause);
    }

}
