package com.ocutrin.functional;

import com.ocutrin.functional.exception.FunctionalException;

/**
 * @author ocutrin
 *
 * @param <T>
 * @param <U>
 * @param <E>
 */
@FunctionalInterface
public interface BiConsumerEx<T, U, E extends FunctionalException> {

    /**
     * @param t
     * @param u
     * @throws E
     */
    void acceptEx(T t, U u) throws E;
}
