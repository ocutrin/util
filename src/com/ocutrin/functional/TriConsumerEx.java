package com.ocutrin.functional;

import com.ocutrin.functional.exception.FunctionalException;

/**
 * @author ocutrin
 *
 * @param <T>
 * @param <U>
 * @param <S>
 * @param <E>
 */
@FunctionalInterface
public interface TriConsumerEx<T, U, S, E extends FunctionalException> {
    /**
     * @param t
     * @param u
     * @param s
     * @throws E
     */
    void acceptEx(T t, U u, S s) throws E;
}
