package com.ocutrin.functional;

/**
 * @author ocutrin
 * @param <T>
 * @param <U>
 * @param <S>
 *
 */
public interface TriConsumer<T, U, S> {
    /**
     * @param t
     * @param u
     * @param s
     */
    void accept(T t, U u, S s);

}
