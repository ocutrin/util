package com.ocutrin.functional;

import com.ocutrin.functional.exception.FunctionalException;

/**
 * @author ocutrin
 *
 * @param <T>
 * @param <S>
 * @param <E>
 */
public interface FunctionEx<T, S, E extends FunctionalException> {

    /**
     * @param t
     * @return S
     * @throws E
     */
    S apply(T t) throws E;
}
