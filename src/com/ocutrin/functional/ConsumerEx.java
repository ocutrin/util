package com.ocutrin.functional;

import com.ocutrin.functional.exception.FunctionalException;

/**
 * @author ocutrin
 *
 * @param <T>
 * @param <E>
 */
@FunctionalInterface
public interface ConsumerEx<T, E extends FunctionalException> {
    /**
     * @param t
     * @throws E
     */
    void acceptEx(T t) throws E;
}
