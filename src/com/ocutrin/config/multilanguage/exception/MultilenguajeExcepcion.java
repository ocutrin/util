package com.ocutrin.config.multilanguage.exception;

public class MultilenguajeExcepcion extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 3224534596046042427L;

    public MultilenguajeExcepcion() {
        super();
    }

    public MultilenguajeExcepcion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public MultilenguajeExcepcion(String message, Throwable cause) {
        super(message, cause);
    }

    public MultilenguajeExcepcion(String message) {
        super(message);
    }

    public MultilenguajeExcepcion(Throwable cause) {
        super(cause);
    }

}
