package com.ocutrin.config.multilanguage.exception;

public class DiccionarioExcepcion extends MultilenguajeExcepcion {

    /**
     *
     */
    private static final long serialVersionUID = 2422880168209173312L;

    public DiccionarioExcepcion() {
        super();
    }

    public DiccionarioExcepcion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public DiccionarioExcepcion(String message, Throwable cause) {
        super(message, cause);
    }

    public DiccionarioExcepcion(String message) {
        super(message);
    }

    public DiccionarioExcepcion(Throwable cause) {
        super(cause);
    }

}
