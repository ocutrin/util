package com.ocutrin.config.multilanguage.exception;

public class DictionaryError extends MultilenguajeError {

    /**
     *
     */
    private static final long serialVersionUID = -4349033894395056761L;

    public DictionaryError() {
        super();
    }

    public DictionaryError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public DictionaryError(String message, Throwable cause) {
        super(message, cause);
    }

    public DictionaryError(String message) {
        super(message);
    }

    public DictionaryError(Throwable cause) {
        super(cause);
    }

}
