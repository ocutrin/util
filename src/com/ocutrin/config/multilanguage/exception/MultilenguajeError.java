package com.ocutrin.config.multilanguage.exception;

public class MultilenguajeError extends Error {

    /**
     *
     */
    private static final long serialVersionUID = -6213891847762061108L;

    public MultilenguajeError() {
        super();
    }

    public MultilenguajeError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public MultilenguajeError(String message, Throwable cause) {
        super(message, cause);
    }

    public MultilenguajeError(String message) {
        super(message);
    }

    public MultilenguajeError(Throwable cause) {
        super(cause);
    }

}
