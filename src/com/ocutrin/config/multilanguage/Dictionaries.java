package com.ocutrin.config.multilanguage;

import java.util.LinkedHashMap;

/**
 * @author ocutrin
 * @param <E>
 *
 */
public class Dictionaries<E> extends LinkedHashMap<E, Dictionary> {

    /**
     *
     */
    private static final long serialVersionUID = -3034592087462637375L;

}
