package com.ocutrin.config.multilanguage;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.ocutrin.config.multilanguage.exception.MultilenguajeError;

/**
 * @author ocutrin
 *
 */
public class MultiLanguage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1664628368969504977L;
    /**
     *
     */
    private final Map<String, Dictionary> dictionaries = new HashMap<>();

    /**
     *
     */
    public MultiLanguage() {
    }

    /**
     * @param dictionaries
     * @throws MultilenguajeError
     */
    public MultiLanguage(final Dictionary... dictionaries) {
	Objects.nonNull(dictionaries);
	for (final Dictionary d : dictionaries) {
	    addDictionary(d);
	}
    }

    /**
     * @param dictionary
     * @throws MultilenguajeError
     */
    public void addDictionary(final Dictionary dictionary) {
	Objects.nonNull(dictionary);
	if (dictionaries.containsKey(dictionary.getLanguage()))
	    throw new MultilenguajeError("Dictionary already exist.");
	dictionaries.put(dictionary.getLanguage(), dictionary);
    }

    /**
     * @param dictionary
     * @return Dictionary
     */
    public Dictionary getDictionary(final String dictionary) {
	return dictionaries.get(dictionary);
    }

    /**
     * @param dictionary
     */
    public void removeDictionary(final String dictionary) {
	dictionaries.remove(dictionary);
    }

    /**
     *
     */
    public void removeAll() {
	dictionaries.clear();
    }

    /**
     * @param dictionary
     * @return boolean
     */
    public boolean exist(final String dictionary) {
	return dictionaries.containsKey(dictionary);
    }

    /**
     * @param dictionary
     *
     */
    public void updateDictionary(final String dictionary) {
	// actualizar diccionario, (volver a mapear todas las palabras).
    }

    /**
     *
     */
    public void updateAll() {
	// dictionaries.entrySet().forEach(e -> {
	// if (e.getValue().isChanges())
	//
	// });

    }

}
