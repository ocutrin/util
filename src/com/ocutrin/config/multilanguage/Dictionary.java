package com.ocutrin.config.multilanguage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.json.JSONObject;

import com.ocutrin.config.Key;
import com.ocutrin.config.multilanguage.exception.DictionaryError;

import lombok.Getter;
import lombok.Setter;

/**
 * @author ocutrin
 *
 */
public class Dictionary implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5854235699779838995L;

    /**
     *
     */
    private Locale locale;

    /**
     *
     */
    private final Map<Key, String> words = new LinkedHashMap<>();

    /**
     *
     */
    @Setter
    @Getter
    private boolean checkRepeatedWords = false;

    /**
     *
     */
    @Getter
    private final boolean changes = false;

    /**
     * @param language
     */
    public Dictionary(final String language) {
	init(language, "", "");
    }

    /**
     * @param language
     * @param country
     */
    public Dictionary(final String language, final String country) {
	init(language, country, "");
    }

    /**
     * @param language
     * @param country
     * @param variante
     */
    public Dictionary(final String language, final String country, final String variante) {
	init(language, country, variante);
    }

    /**
     * @param locale
     */
    public Dictionary(final Locale locale) {
	Objects.nonNull(locale);
	this.locale = locale;
    }

    private final void init(final String language, final String country, final String variante) {
	Objects.nonNull(language);
	Objects.nonNull(country);
	Objects.nonNull(variante);
	locale = new Locale(language, country, variante);
	load();
    }

    /**
     *
     */
    public void load() {
	// to overwrite
    }

    /**
     * @param key
     * @return String
     */
    public String getWord(final Key key) {
	return words.get(key);
    }

    /**
     * @return String
     */
    public String getLanguage() {
	return locale.getLanguage();
    }

    /**
     * @return String
     */
    public String getCountry() {
	return locale.getCountry();
    }

    /**
     * @return String
     */
    public String getVariant() {
	return locale.getVariant();
    }

    /**
     *
     */
    public void log() {
	System.out
		.println("Dictionary: " + locale.getLanguage() + " " + locale.getCountry() + " " + locale.getVariant());
	words.forEach((c, v) -> System.out.println(c.getKey() + "=" + v));
    }

    /**
     * @param key
     * @return boolean
     */
    public boolean keyExist(final Key key) {
	return words.get(key) != null;
    }

    /**
     * @param word
     * @return boolean
     */
    public boolean wordExist(final String word) {
	return words.values().stream().filter(p -> p.equals(word)).findFirst().isPresent();
    }

    /**
     * @param key
     * @param word
     * @return boolean
     */
    public boolean addWord(final Key key, final String word) {
	checkIfExistKey(key);
	if (checkRepeatedWords)
	    checkIfExistWord(word);
	return words.put(key, word) != null;
    }

    private void checkIfExistKey(final Key key) {
	if (keyExist(key))
	    throw new DictionaryError("Key already exist.");
    }

    private void checkIfExistWord(final String word) {
	if (wordExist(word))
	    throw new DictionaryError("Word already exist.");
    }

    /**
     * @return boolean
     */
    public boolean validarPalabrasRepetidas() {
	final Set<String> set = new HashSet<>();
	return !words.entrySet().stream().filter(e -> !set.add(e.getValue())).findFirst().isPresent();
    }

    /**
     * @return Map<key, String>
     */
    public Map<String, List<Key>> getRepeatedWords() {
	final Map<String, Key> unicos = new LinkedHashMap<>();
	final Map<String, List<Key>> repetidos = new LinkedHashMap<>();
	words.entrySet().forEach(e -> searchRepeated(unicos, repetidos, e));
	return repetidos;
    }

    private void searchRepeated(final Map<String, Key> unique, final Map<String, List<Key>> repeated,
	    final Map.Entry<Key, String> e) {
	List<Key> keysList;
	if (unique.putIfAbsent(e.getValue(), e.getKey()) != null) {
	    if ((keysList = repeated.get(e.getValue())) == null) {
		keysList = new ArrayList<>();
		keysList.add(unique.get(e.getValue()));
	    }
	    keysList.add(e.getKey());
	    repeated.put(e.getValue(), keysList);
	}
    }

    /**
     * @return JSONObject
     */
    public JSONObject converToJSONObject() {

	final Map<String, String> listAux = new LinkedHashMap<>();
	for (final Map.Entry<Key, String> e : words.entrySet()) {
	    listAux.put(e.getKey().getKey(), e.getValue());
	}
	final JSONObject wordsJSON = new JSONObject(listAux);

	final JSONObject localeJSON = new JSONObject();
	localeJSON.put("language", locale.getLanguage());
	localeJSON.put("country", locale.getCountry());
	localeJSON.put("variant", locale.getVariant());

	final JSONObject jsonObject = new JSONObject();
	jsonObject.put("locale", localeJSON);
	jsonObject.put("words", wordsJSON);
	return jsonObject;
    }

    /**
     * @throws IOException
     */
    public void writeDictionaryToFile() throws IOException {
	final File file = new File("test/example.json");
	if (!file.exists())
	    file.createNewFile();
	final FileWriter fw = new FileWriter(file);
	fw.write(converToJSONObject().toString());
	fw.close();
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (locale.getLanguage() == null ? 0 : locale.getLanguage().hashCode());
	result = prime * result + (locale.getCountry() == null ? 0 : locale.getCountry().hashCode());
	result = prime * result + (locale.getVariant() == null ? 0 : locale.getVariant().hashCode());
	return result;
    }

    @Override
    public boolean equals(final Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof Dictionary))
	    return false;
	final Dictionary other = (Dictionary) obj;
	if (locale.getLanguage() == null) {
	    if (other.locale.getLanguage() != null)
		return false;
	} else if (!locale.getLanguage().equals(other.locale.getLanguage()))
	    return false;
	if (locale.getCountry() == null) {
	    if (other.locale.getCountry() != null)
		return false;
	} else if (!locale.getCountry().equals(other.locale.getCountry()))
	    return false;
	if (locale.getVariant() == null) {
	    if (other.locale.getVariant() != null)
		return false;
	} else if (!locale.getVariant().equals(other.locale.getVariant()))
	    return false;
	return true;
    }

}
