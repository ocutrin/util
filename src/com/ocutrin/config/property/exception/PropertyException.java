package com.ocutrin.config.property.exception;

/**
 * @author ocutrin
 *
 */
public class PropertyException extends Error {

    /**
     *
     */
    private static final long serialVersionUID = -9066198488597618618L;

    /**
     *
     */
    public PropertyException() {
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public PropertyException(final String message, final Throwable cause, final boolean enableSuppression,
	    final boolean writableStackTrace) {
	super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * @param message
     * @param cause
     */
    public PropertyException(final String message, final Throwable cause) {
	super(message, cause);
    }

    /**
     * @param message
     */
    public PropertyException(final String message) {
	super(message);
    }

    /**
     * @param cause
     */
    public PropertyException(final Throwable cause) {
	super(cause);
    }

}
