package com.ocutrin.config.property;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

import org.json.JSONObject;

import com.ocutrin.config.property.exception.PropertyException;

/**
 * @author ocutrin
 *
 */
public final class PropertiesHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8822975541005386812L;

    /*
    *
    */
    private static final String DEFAULT_FILE = "application.properties";

    /*
    *
    */
    private static final String DEFAULT_PATH = "resources/config/";

    /*
     *
     */
    private static boolean load = false;

    /*
     *
     */
    private static Properties prop = new Properties();

    // TODO poner flag para cuando al guardar solo guarder si han habiado cambios.

    /*
     *
     */
    private static String loadFile;

    /*
     *
     */
    private static String loadPath;

    /**
     * @throw PropertyException
     */
    public static void load() {
	load(DEFAULT_FILE);
    }

    /**
     * @param fileProperties
     * @throw PropertyException
     */
    public static void load(final String fileProperties) {
	load(DEFAULT_PATH, fileProperties);
    }

    /**
     * @param filePath
     * @param fileProperties
     * @throw PropertyException
     */
    public static void load(final String filePath, final String fileProperties) {
	Objects.nonNull(filePath);
	Objects.nonNull(fileProperties);
	loadAgainCheck();
	try {
	    prop.load(new FileInputStream(filePath + fileProperties));
	} catch (final IOException e) {
	    throw new PropertyException("Not such file properties " + filePath + fileProperties + ".");
	}
	loadFile = fileProperties;
	loadPath = filePath;
	load = true;
    }

    /**
     * @throw PropertyException
     */
    public static void reload() {
	loadCheck();
	load(loadPath, loadFile);
    }

    private static void loadAgainCheck() {
	if (load)
	    throw new PropertyException(
		    "Properties helper is already loaded, use reload(). If you want to overwrite, first use the clean method.");
    }

    /**
     * @throw PropertyException
     */
    public static void save() {
	save(loadPath, loadFile, null);
    }

    /**
     * @param comments
     * @throw PropertyException
     */
    public static void save(final String comments) {
	save(loadPath, loadFile, comments);
    }

    /**
     * @param fileProperties
     * @param comments
     * @throw PropertyException
     */
    public static void save(final String fileProperties, final String comments) {
	save(loadPath, fileProperties, comments);
    }

    /**
     * @param filePath
     * @param fileProperties
     * @param comments
     * @throw PropertyException
     */
    public static void save(final String filePath, final String fileProperties, final String comments) {
	loadCheck();
	Objects.nonNull(filePath);
	Objects.nonNull(fileProperties);
	Objects.nonNull(comments);
	try {
	    prop.store(new FileOutputStream(filePath + fileProperties), comments);
	} catch (final IOException e) {
	    throw new PropertyException("Not such file properties " + filePath + fileProperties + ".");
	}
    }

    /**
     * @param property
     * @return Object
     * @throw PropertyException
     */
    public static Object get(final String property) {
	Objects.nonNull(property);
	loadCheck();
	final Object p = prop.get(property);
	if (p == null)
	    throw new PropertyException("Property not exist.");
	return p;
    }

    /**
     * @param property
     * @return String
     * @throw PropertyException
     */
    public static String getString(final String property) {
	return (String) get(property);
    }

    /**
     * @param property
     * @return Integer
     * @throw PropertyException
     */
    public static int getInt(final String property) {
	try {
	    return Integer.parseInt((String) get(property));
	} catch (final NumberFormatException e) {
	    throw new PropertyException(errorTypeMesssage(Integer.TYPE));
	}
    }

    /**
     * @param property
     * @return Double
     * @throw PropertyException
     */
    public static double getDouble(final String property) {
	try {
	    return Double.parseDouble((String) get(property));
	} catch (final NumberFormatException e) {
	    throw new PropertyException(errorTypeMesssage(Double.TYPE));
	}
    }

    /**
     * @param property
     * @return List<String>
     * @throw PropertyException
     */
    public static List<String> getListString(final String property) {
	return Arrays.asList(getString(property).split(","));
    }

    /**
     * @param property
     * @return List<Integer>
     * @throw PropertyException
     */
    public static List<Integer> getListInt(final String property) {
	try {
	    return Arrays.asList(getString(property).split(",")).stream().map(m -> Integer.parseInt(m))
		    .collect(Collectors.toList());
	} catch (final NumberFormatException e) {
	    throw new PropertyException(errorTypeMesssage(Integer.TYPE));
	}
    }

    /**
     * @param property
     * @return List<Double>
     * @throw PropertyException
     */
    public static List<Double> getListDouble(final String property) {
	try {
	    return Arrays.asList(getString(property).split(",")).stream().map(m -> Double.parseDouble(m))
		    .collect(Collectors.toList());
	} catch (final NumberFormatException e) {
	    throw new PropertyException(errorTypeMesssage(Double.TYPE));
	}
    }

    /**
     * @param property
     * @return JSONObject
     * @throw PropertyException
     */
    public static JSONObject getJSONObject(final String property) {
	try {
	    return new JSONObject(getString(property));
	} catch (final org.json.JSONException e) {
	    throw new PropertyException(errorTypeMesssage(JSONObject.class));
	}
    }

    private static String errorTypeMesssage(final Class<?> type) {
	return "Property is not " + type + " type.";
    }

    /**
     * @param property
     * @param value
     * @throw PropertyException
     */
    public static void set(final String property, final String value) {
	loadCheck();
	prop.put(property, value);
    }

    /**
     * @param property
     * @param value
     * @throw PropertyException
     */
    public static void setIfExist(final String property, final String value) {
	try {
	    getString(property);// throws if not exist
	    set(property, value);
	} catch (final PropertyException e) {
	    // no set if not exist.
	}
    }

    /**
     * @param property
     * @param value
     * @throw PropertyException
     */
    public static void setIfNotExist(final String property, final String value) {
	try {
	    getString(property);
	} catch (final PropertyException e) {
	    set(property, value);
	}
    }

    /**
     * @throw PropertyException
     */
    public static void log() {
	loadCheck();
	prop.list(System.out);
    }

    private static void loadCheck() {
	if (!load)
	    throw new PropertyException("Not any load properties.");
    }

    /**
    *
    */
    public void clear() {
	prop.clear();
	load = false;
	loadFile = null;
	loadPath = null;
    }

}
