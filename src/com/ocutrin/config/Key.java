package com.ocutrin.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.json.JSONObject;

import lombok.Getter;
import lombok.Setter;

/**
 * @author ocutrin
 *
 */
public class Key implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6606636397406913318L;

    /**
    *
    */
    @Getter
    @Setter
    private String name;

    /**
     *
     */
    @Getter
    @Setter
    private String clazz;

    /**
    *
    */
    @Getter
    @Setter
    private String paCkage;

    /**
     *
     */
    @Getter
    @Setter
    private String project;

    /**
     */
    public Key() {
	init("", "", "", "");
    }

    /**
     * @param name
     */
    public Key(final String name) {
	init(name, "", "", "");
    }

    /**
     * @param name
     * @param clazz
     */
    public Key(final String name, final String clazz) {
	init(name, clazz, "", "");
    }

    /**
     * @param name
     * @param clazz
     * @param paCkage
     */
    public Key(final String name, final String clazz, final String paCkage) {
	init(name, clazz, paCkage, "");
    }

    /**
     *
     * @param name
     * @param clazz
     * @param paCkage
     * @param project
     */
    public Key(final String name, final String clazz, final String paCkage, final String project) {
	init(name, clazz, paCkage, project);
    }

    private void init(final String name, final String clazz, final String paCkage, final String project) {
	checkNulls(name, clazz, paCkage, project);
	this.name = toLowerCase(name);
	this.clazz = toLowerCase(clazz);
	this.paCkage = toLowerCase(paCkage);
	this.project = toLowerCase(project);
    }

    private void checkNulls(final String name, final String clazz, final String paCkage, final String project) {
	Objects.nonNull(name);
	Objects.nonNull(clazz);
	Objects.nonNull(paCkage);
	Objects.nonNull(project);
    }

    private String toLowerCase(final String s) {
	return s.toLowerCase();
    }

    /**
     *
     * @return String
     */
    public String getKey() {
	return getRoute().stream().collect(Collectors.joining("."));
    }

    /**
     * @return List<String>
     */
    public List<String> getRoute() {
	return addIfExis(addIfExis(addIfExis(addIfExis(new ArrayList<>(), name), clazz), paCkage), project);
    }

    private List<String> addIfExis(final List<String> l, final String s) {
	if (s != null && !s.isEmpty()) {
	    l.add(s);
	}
	return l;
    }

    /**
     * @return JSONObject
     */
    public JSONObject toJSONObject() {
	final JSONObject js = new JSONObject();
	if (!name.isEmpty())
	    js.put("name", name);
	if (!clazz.isEmpty())
	    js.put("class", clazz);
	if (!paCkage.isEmpty())
	    js.put("paCkage", paCkage);
	if (!project.isEmpty())
	    js.put("project", project);
	return js;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (clazz == null ? 0 : clazz.hashCode());
	result = prime * result + (name == null ? 0 : name.hashCode());
	result = prime * result + (paCkage == null ? 0 : paCkage.hashCode());
	result = prime * result + (project == null ? 0 : project.hashCode());
	return result;
    }

    @Override
    public boolean equals(final Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof Key))
	    return false;
	final Key other = (Key) obj;
	if (clazz == null) {
	    if (other.clazz != null)
		return false;
	} else if (!clazz.equals(other.clazz))
	    return false;
	if (name == null) {
	    if (other.name != null)
		return false;
	} else if (!name.equals(other.name))
	    return false;
	if (paCkage == null) {
	    if (other.paCkage != null)
		return false;
	} else if (!paCkage.equals(other.paCkage))
	    return false;
	if (project == null) {
	    if (other.project != null)
		return false;
	} else if (!project.equals(other.project))
	    return false;
	return true;
    }

}
