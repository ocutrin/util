package com.ocutrin.collection.exception;

/**
 * @author ocutrin
 *
 */
public class ListPlusException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -6433095562483750190L;

    /**
     * 
     */
    public ListPlusException() {
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public ListPlusException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
	super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * @param message
     * @param cause
     */
    public ListPlusException(String message, Throwable cause) {
	super(message, cause);
    }

    /**
     * @param message
     */
    public ListPlusException(String message) {
	super(message);
    }

    /**
     * @param cause
     */
    public ListPlusException(Throwable cause) {
	super(cause);
    }

}
