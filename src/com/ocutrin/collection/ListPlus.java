package com.ocutrin.collection;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import com.ocutrin.collection.exception.ListPlusException;
import com.ocutrin.functional.BiConsumerEx;
import com.ocutrin.functional.exception.FunctionalException;

/**
 * @author ocutrin
 *
 * @param <E>
 */
public class ListPlus<E> extends ArrayList<E> {

    /**
     *
     */
    private static final long serialVersionUID = 9189727315272131397L;

    /**
     *
     */
    public ListPlus() {
	super();
    }

    /**
     * @param initialCapacity
     */
    public ListPlus(final int initialCapacity) {
	super(initialCapacity);
    }

    /**
     * @param e
     */
    @SafeVarargs
    public ListPlus(final E... e) {
	super(Arrays.asList(e));
    }

    /**
     * @param listsPlus
     */
    @SafeVarargs
    public ListPlus(final Collection<E>... listsPlus) {
	super(Arrays.asList(listsPlus).stream().flatMap(Collection<E>::stream).collect(Collectors.toList()));
    }

    /**
     * @param action
     */
    public void forEach(final BiConsumer<Integer, ? super E> action) {
	try {
	    basicForEach(action, (i, elementData) -> action.accept(i, elementData[i]));
	} catch (final ListPlusException e) {
	    // do nothing, there is no possibility of exception.
	}
    }

    /**
     * @param action
     * @throws ListPlusException
     */
    public void forEachEx(final BiConsumerEx<Integer, ? super E, ? extends FunctionalException> action)
	    throws ListPlusException {
	basicForEach(action, (i, elementData) -> {
	    action.acceptEx(i, elementData[i]);
	});
    }

    private void basicForEach(final Object action,
	    final BiConsumerEx<Integer, E[], ? extends FunctionalException> action2) throws ListPlusException {
	Objects.requireNonNull(action);
	final int expectedModCount = modCount;
	@SuppressWarnings("unchecked")
	final E[] elementData = (E[]) getObjectFromSupperClass("elementData");
	final int size = size();
	for (int i = 0; modCount == expectedModCount && i < size; i++) {
	    try {
		action2.acceptEx(i, elementData);
	    } catch (final FunctionalException e) {
		throw new ListPlusException(e);
	    }
	}
	checkModCount(expectedModCount);
    }

    private void checkModCount(final int expectedModCount) {
	if (modCount != expectedModCount) {
	    throw new ConcurrentModificationException();
	}
    }

    private Object getObjectFromSupperClass(final String fieldName) {
	return getObjectFromField(fieldName);
    }

    private Object getObjectFromField(final String fieldName) {
	try {
	    final Field field = getClass().getSuperclass().getDeclaredField(fieldName);
	    if (!field.isAccessible())
		field.setAccessible(true);
	    return field.get(this);
	} catch (final NoSuchFieldException e) {
	    throw new NoSuchFieldError();
	} catch (final IllegalAccessException e) {
	    throw new IllegalAccessError();
	}
    }

}