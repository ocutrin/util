package com.ocutrin.test;

import com.ocutrin.config.Key;
import com.ocutrin.config.multilanguage.Dictionary;

/**
 * @author ocutrin
 *
 */
public class Dictionary_Es extends Dictionary {

    /**
     *
     */
    private static final long serialVersionUID = 465916401917746562L;

    /**
    *
    */
    public Dictionary_Es() {
	super("es", "ES");
    }

    @Override
    public void load() {
	addWord(new Key("nombre"), "Nombre");
	addWord(new Key("lastname"), "Lastname");
	addWord(new Key("title"), "Lista de mantenimiento");
	addWord(new Key("barca"), "Barca de colores");
    }

}
