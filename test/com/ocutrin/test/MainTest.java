package com.ocutrin.test;

import java.io.IOException;

import org.junit.Test;

import com.ocutrin.config.multilanguage.MultiLanguage;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author ocutrin
 *
 */
@SuppressWarnings("unused")
public class MainTest extends Application {

    /**
     * @param args
     */
    @Test
    public static void main(final String[] args) {
	launch(args);
    }

    @Override
    public void start(final Stage primaryStage) {

	//// final Dictionary dicc1 = new Dictionary("ES", "es", "es");
	// final Dictionary dicc2 = new Dictionary("en", "en", "en");

	final MultiLanguage ml = new MultiLanguage();

	ml.addDictionary(new Dictionary_Es());

	System.out.println(ml.getDictionary("es").converToJSONObject().toString());

	try {
	    ml.getDictionary("es").writeDictionaryToFile();
	} catch (final IOException e) {
	    e.printStackTrace();
	}

	System.exit(0);

    }

    private static void properties() {
	// PropertiesHelper.load();

	// PropertiesHelper.log();

	// PropertiesHelper.getListString("timer").forEach(System.out::println);

	// PropertiesHelper.getPropertyListInt("timer").forEach(System.out::println);

	// PropertiesHelper.setProperty("t", "ss");

	// PropertiesHelper.setIfExist("timer", "3");

	// PropertiesHelper.save();

	// System.out.println(PropertiesHelper.getString("timer"));

	// System.out.println(PropertiesHelper.getJSONObject("timer"));

	// System.out.println(PropertiesHelper.getJSONObject("timer"));

	// System.out.println(PropertiesHelper.getPropertyDouble("timer") + "");
    }

}
